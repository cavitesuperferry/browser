var url  = require('url'),
    express = require('express'),
    path = require('path')
    http = require('http');
	fs = require('fs');

var app = express();
var server = http.createServer(app);

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'data')));
app.use(express.static(path.join(__dirname, 'app')));
app.set('views', __dirname + '/app/views');
app.set('view engine', 'ejs');

app.get('/', function(req, res){
    res.render('home',{});
});

var PORT = 33334;
var HOST = '52.221.225.48';
var dgram = require('dgram');
var client = dgram.createSocket('udp4');

var interval = setInterval(function() {
  fs.readFile('data/output.txt', function(err, data) {
		var array = data.toString().split("\n");
		var counts = array.length;
		console.log(array[counts-2]);
		var message = new Buffer(array[counts-2]);
		client.send(message, 0, message.length, PORT, HOST, function(){
			if(err) throw err;
			console.log("UPD message sent to " + HOST + ":" + PORT);
		});
		
});
},1000);



app.listen(4005)
console.log('server running 4005');
