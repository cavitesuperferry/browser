#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>

typedef struct{
	
	unsigned long itow;	
	unsigned short  year;
	unsigned char month;
	unsigned char day;
	unsigned char valid;
	unsigned char hour;
	unsigned char min;
	unsigned char sec;	
	unsigned long tAcc;
	signed long nano;
	unsigned char fixType;
	unsigned char numSV;
	signed long lon;
	signed long lat;
	signed long height;
	signed long hMSL;
	unsigned long hAcc;
	unsigned long vAcc;
	signed long velN;
	signed long velE;
	signed long velD;
	signed long gSpeed;
	signed long headMot;
	unsigned long sAcc;
	unsigned short pDOP;
	signed long headVeh;
	
} rec_nav_sol;

typedef struct{
	
	int xgyro, ygyro, zgyro;
	int xaccel, yaccel, zaccel;
	unsigned char typeXgyro,typeYgyro,typeZgyro;
	unsigned char typeXaccel,typeYaccel,typeZaccel;
	unsigned long sTtag;
	int payload_length;
	
} rec_esf_raw;

void twoDataswap(int *inputx2);
void threeDataswap(int *inputx3); 
int fourDataswap(int *inputx4);
int L2B_ENDIAN(int L_Endian, int byte_cnt);

int main(void)
{
		
	int esfcounter=0;
	int counter;
	int counter2=0;
    int sockfd = 0;
    int bytesReceived = 0;
    char recvBuff[256];
    memset(recvBuff, '0', sizeof(recvBuff));
    struct sockaddr_in serv_addr;
    int data_temp;
    int data_pos=0;
    rec_nav_sol record;
	rec_esf_raw esf;

    /* Create a socket first */
    if((sockfd = socket(AF_INET, SOCK_STREAM, 0))< 0)
    {
        printf("\n Error : Could not create socket \n");
        return 1;
    }

    /* Initialize sockaddr_in data structure */
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(4001); // port
    serv_addr.sin_addr.s_addr = inet_addr("192.168.1.17");

    /* Attempt a connection */
    if(connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr))<0)
    {
        printf("\n Error : Connect Failed \n");
        return 1;
    }

    /* Create file where data will be stored */
    FILE *fp;
    fp = fopen("../data/output.txt", "ab"); 
    //printf("fp = %d\n", &fp);
    if(fp == NULL)
    {
        printf("Error opening file");
        return 1;
    }

  
    while((bytesReceived = read(sockfd, recvBuff, 700)) > 0)
   
    {
		

		data_temp = L2B_ENDIAN(*(int *)(&recvBuff[0]),sizeof(int)); //results in 0xB5621007
        
//start nav_sol
        if (data_temp == 0xB5620107)
        {
			
			for(data_pos = 0; data_pos < 256; data_pos++){
				
					record.itow = (*(int*)&recvBuff[data_pos + 6]);
					record.year = (((*(int*)&recvBuff[data_pos + 10])<<16)>>16);					
					record.month = (*(int*)&recvBuff[data_pos + 10])>>16;
					record.day = (*(int*)&recvBuff[data_pos + 10])>>24;
					record.hour = ((*(int*)&recvBuff[data_pos + 14])<<24)>>24;
					record.min = ((*(int*)&recvBuff[data_pos + 14])<<16)>>24;
					record.sec = ((*(int*)&recvBuff[data_pos + 14])<<8)>>24;
					record.valid = (*(int*)&recvBuff[data_pos + 14])>>24;
					record.nano = (*(int*)&recvBuff[data_pos + 22]);
					record.fixType = (*(int*)&recvBuff[data_pos + 26]);
					record.numSV = (*(int*)&recvBuff[data_pos + 29]);
					record.lon = (*(int*)&recvBuff[data_pos + 30]);  
					record.lat = (*(int*)&recvBuff[data_pos + 34]);
				
					
					//printf("record itow = %x\t %d\n", record.itow,record.itow);
					//printf("record year = %x\t %d\n", record.year,record.year);
					//printf("record month = %x\t %d\n", record.month,record.month);
					//printf("record day = %x\t %d\n", record.day,record.day);
					//printf("record hour = %x\t %d\n", record.hour,record.hour);
					//printf("record min = %x\t %d\n", record.min,record.min);
					//printf("record sec = %x\t %d\n", record.sec,record.sec);
					//printf("record valid = %x\t %d\n", record.valid,record.valid);
					//printf("record nano = %x\t %0.1f\n", record.nano,(float)record.nano/1000000000);
					//printf("record fixType = %x\t %d\n", record.fixType,record.fixType);
					//printf("record numSV = %x\t %d\n", record.numSV,record.numSV);
					//printf("record longitude = %x\t %f\n", record.lon,(float)record.lon/10000000); 
					//printf("record latitude = %x\t %f\n", record.lat,(float)record.lat/10000000);
					
					
					fprintf(fp,"pos_data[%d] %02d:%02d:%04d:%02d:%02d:%02d:%0.1f %.10f %.10f %d\t \n",data_pos,record.month ,record.day,record.year,record.hour ,record.min,record.sec,(float)record.nano/1000000000,(float)record.lat/10000000,(float)record.lon/10000000,record.fixType );
					fflush(fp);
					//fprintf(fp,"%g\n",record.year);
					
					
					
					break;
					
			
			
			
			//printf("record itow[%d] = %08x\t %d\n", data_pos,record.lat,record.lat);
			//printf("record year[%d] = %08x\t %d\n", data_pos,record.year,record.year);
       // printf("Bytes received %08x\n",bytesReceived); 
      // printf("Bytes received %08x\n",recvBuff[0]); 
        
        
           
        // recvBuff[n] = 0;
        //fwrite(recvBuff, 1,bytesReceived,fp);
        // printf("%s \n", recvBuff);
        
			} // end_inner_for_loop
			
		//else
		//printf("No data:\n"); 
		
		} //end_if_nav_sol
		
// start esf
		
		if (data_temp == 0xB5621003)
        {
		
			for(data_pos = 0; data_pos < 700; data_pos++){
			
			for (int i=0;i<10;i++){
				
				{
					
					//printf("%08x\n", (int *)&recvBuff[data_pos]);
				//accelerometr x, data type and sensor time tag						
					esf.xaccel = (((*(int*)&recvBuff[data_pos + 10+esfcounter])<<8)>>8);
					esf.typeXaccel=(*(int*)&recvBuff[data_pos + 10+esfcounter])>>24;					
					esf.sTtag = (*(int*)&recvBuff[data_pos + 14+esfcounter]);
					
					
				//accelerometr y, data type and sensor time tag		
					esf.yaccel = (((*(int*)&recvBuff[data_pos + 18+esfcounter])<<8)>>8);
					esf.typeYaccel=(*(int*)&recvBuff[data_pos + 18+esfcounter])>>24;					
					esf.sTtag = (*(int*)&recvBuff[data_pos + 22+esfcounter]);
					
					
				//accelerometr z, data type and sensor time tag		
					esf.zaccel = (((*(int*)&recvBuff[data_pos + 26+esfcounter])<<8)>>8);
					esf.typeZaccel=(*(int*)&recvBuff[data_pos + 26+esfcounter])>>24;					
					esf.sTtag = (*(int*)&recvBuff[data_pos + 30+esfcounter]);
					
					
				//gyroscope x, data type and sensor time tag		
					esf.xgyro = (((*(int*)&recvBuff[data_pos + 34+esfcounter])<<8)>>8);
					//esf.typeXgyro=(*(int*)&recvBuff[data_pos + 34+esfcounter])>>24;					
					//esf.sTtag = (*(int*)&recvBuff[data_pos + 38+esfcounter]);
					
				//gyroscope y, data type and sensor time tag		
					esf.ygyro = (((*(int*)&recvBuff[data_pos + 42+esfcounter])<<8)>>8);
					esf.typeYgyro=(*(int*)&recvBuff[data_pos + 42+esfcounter])>>24;					
					esf.sTtag = (*(int*)&recvBuff[data_pos + 46+esfcounter]);
					
				//gyroscope z, data type and sensor time tag		
					esf.zgyro = (((*(int*)&recvBuff[data_pos + 50+esfcounter])<<8)>>8);
					esf.typeZgyro=(*(int*)&recvBuff[data_pos + 50 + esfcounter])>>24;					
					esf.sTtag = (*(int*)&recvBuff[data_pos + 54+esfcounter]);
					
					
					
											
					//printf("esf payload_length = %x\t %d\n", esf.payload_length,esf.payload_length);
					
					
				//print accelerometr x, data type and sensor time tag
					//printf("esf xaccel = %x\t %0.3f\n",esf.xaccel,(float)esf.xaccel/1024 );
					//printf("esf typeXaccel = %x\n", esf.typeXaccel);
					//printf("esf sTtag = %08x\t %d\n", esf.sTtag,esf.sTtag);
					
					
				//print accelerometr y, data type and sensor time tag					
					//printf("esf yaccel = %x\t %0.3f\n",esf.yaccel,(float)esf.yaccel/1024 );
					//printf("esf typeYaccel = %x\n", esf.typeYaccel);
					//printf("esf sTtag = %08x\t %d\n", esf.sTtag,esf.sTtag);
					
				//print accelerometr z, data type and sensor time tag					
					//printf("esf zaccel = %x\t %0.3f\n",esf.zaccel,(float)esf.zaccel/1024 );
					//printf("esf typeZaccel = %x\n", esf.typeZaccel);
					//printf("esf sTtag = %08x\t %d\n", esf.sTtag,esf.sTtag);
					
				//print gyroscope x, data type and sensor time tag					
					//printf("esf xgyro = %x\t %0.3f\n",esf.xgyro,(float)esf.xgyro/4096 );
					//printf("esf typeXgyro = %x\n", esf.typeXgyro);
					//printf("esf sTtag = %08x\t %d\n", esf.sTtag,esf.sTtag);
				
				//print gyroscope y, data type and sensor time tag					
					//printf("esf ygyro = %08x\t %0.3f\n",esf.ygyro,(float)esf.ygyro/4096 );
					//printf("esf typeYgyro = %x\n", esf.typeYgyro);
					//printf("esf sTtag = %08x\t %d\n", esf.sTtag,esf.sTtag);
					
				//print gyroscope z, data type and sensor time tag					
					//printf("esf zgyro = %08x\t %0.3f\n",esf.zgyro,(float)esf.zgyro/4096 );
					//printf("esf typeZgyro = %x\n", esf.typeZgyro);
					//printf("esf sTtag[%d] = %08x\t %d\n", i,esf.sTtag,esf.sTtag);
					
			//fprintf(fp,"imu raw data\t %d\t %0.3f\t %0.3f\t %0.3f\t %0.3f\t %0.3f\t %0.3f\t \n",esf.sTtag,(float)esf.xaccel/1024,(float)esf.yaccel/1024,(float)esf.zaccel/1024,(float)esf.xgyro/4096,(float)esf.ygyro/4096,(float)esf.zgyro/4096 );
			//fflush(fp);
					
			} // end_innermost_for_loop
					
					
					
					
				esfcounter +=56;				
			
		} // end_inner_for_loop
		
			esfcounter = 0;
					
					break;				
					
						
		
		} // end_outer_for_loop 
		
		
    } // end_if_esf
    
    
	} // end_while_loop
	
	
	
	
    if(bytesReceived < 0)
    {
        printf("\n Read Error \n");
    }


    return 0;
}



int L2B_ENDIAN(int L_Endian, int byte_cnt){

    int B_Endian;

    int ctr = 0;

    for(ctr = 0; ctr < byte_cnt; ctr++){

        B_Endian = (B_Endian << 8) | (L_Endian & 0xFF);

        L_Endian = L_Endian >> 8;

    }

    return B_Endian;

}
