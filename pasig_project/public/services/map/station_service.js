function station(){

	xmlhttpStation.onreadystatechange = function(){

		if(xmlhttpStation.readyState == 4 && xmlhttpStation.status == 200){

			var array = xmlhttpStation.responseText.toString().split("\n");
			var counter = 0;
			var c;
			for(i in array){
				var object = array[i].toString().split(",");
				station_name.push(object[1]);
				station_lat.push(object[4]);
				station_lng.push(object[5]);
				L.marker([object[2], object[3]],{icon: station_marker}).addTo(myradar);
				L.marker([object[2], object[3]],{icon: station_marker}).addTo(mymap).bindPopup("<b>" + object[1] + "</b>");
				//current_destination_lat.push(object[2]);
				//current_destination_lng.push(object[3]);
				//Display point to point coordination with 30meters wide.
				station_circle.push(L.circle([object[2], object[3]], {
					color: 'blue',
					fillColor: '#f03',
					fillOpacity: 0,
					radius: 70
				}).addTo(mymap));
			}
	
		}
	}

	xmlhttpStation.open("GET","file/stations_info/station_info.txt", true);
	xmlhttpStation.send();

}

function CurrentAndNextDestination(){
	for(var x = 0; x < station_circle.length; x++){
		if(markerMap.getLatLng().distanceTo(station_circle[x].getLatLng()) <= station_circle[x].getRadius()){
			station_index = x;
			localStorage.setItem("station_index", x);
			break;
		}	
	}
}