function getPoints(){
	xmlhttpPoint.onreadystatechange = function(){

		if(xmlhttpPoint.readyState == 4 && xmlhttpPoint.status == 200){

			var array = xmlhttpPoint.responseText.toString().split("\n");
			for(i in array){
				var object = array[i].toString().split(",");
				L.circle([object[0], object[1]], {
					color: 'orange',
					fillColor: '#f03',
					fillOpacity: 0,
					radius: 0.1
				}).addTo(mymap);
			}
	
		}
	}

	xmlhttpPoint.open("GET","file/points/points.txt", true);
	xmlhttpPoint.send();
}