function CurGetHeading(lat1,lng1,lat2,lng2){
	if(lng1 > lng2){
		//alert("GOING TO EAST");
		$('#cd').text(station_name[parseInt(localStorage.getItem("station_index")) + 1]);
		$('#nd').text(station_name[parseInt(localStorage.getItem("station_index")) + 2]);		
		$('#ds').text(distance2(lat1, lng1, next_destination_lat[parseInt(localStorage.getItem("station_index")) + 1], next_destination_lng[parseInt(localStorage.getItem("station_index")) + 1], "K") + "Kms");
		$('#ns').text(distance2(lat1, lng1, next_destination_lat[parseInt(localStorage.getItem("station_index")) + 2], next_destination_lng[parseInt(localStorage.getItem("station_index")) + 2], "K") + "Kms");
		$('#sp').text(time(distance2(lat1, lng1, next_destination_lat[parseInt(localStorage.getItem("station_index")) + 1], next_destination_lng[parseInt(localStorage.getItem("station_index")) + 1], "K"),temp_speed));
		$('#nt').text(time(distance2(lat1, lng1, next_destination_lat[parseInt(localStorage.getItem("station_index")) + 2], next_destination_lng[parseInt(localStorage.getItem("station_index")) + 2], "K"),temp_speed));
	} else if(lng1 < lng2){
		//alert("GOING TO WEST");
		$('#cd').text(station_name[parseInt(localStorage.getItem("station_index")) - 1]);
		$('#nd').text(station_name[parseInt(localStorage.getItem("station_index")) - 2]);	
		$('#ds').text(distance2(lat1, lng1, next_destination_lat[parseInt(localStorage.getItem("station_index")) - 1], next_destination_lng[parseInt(localStorage.getItem("station_index")) - 1], "K") + "Kms");
		$('#ns').text(distance2(lat1, lng1, next_destination_lat[parseInt(localStorage.getItem("station_index")) - 2], next_destination_lng[parseInt(localStorage.getItem("station_index")) - 2], "K") + "Kms");
		$('#sp').text(time(distance2(lat1, lng1, next_destination_lat[parseInt(localStorage.getItem("station_index")) - 1], next_destination_lng[parseInt(localStorage.getItem("station_index")) - 1], "K"),temp_speed));
		$('#nt').text(time(distance2(lat1, lng1, next_destination_lat[parseInt(localStorage.getItem("station_index")) - 2], next_destination_lng[parseInt(localStorage.getItem("station_index")) - 2], "K"),temp_speed));
	}
}