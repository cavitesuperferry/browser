//Initialize variables

var polylinePoints = [];
var mymap;
var marker = null;
var first_load = null;
var stations = ["14.6668","120.94395"];
var station_distance = null;
var min_speed = 30; //kph
var time_duration = null;
var started_hour = null;
var hours = 0;
var minutes = 0;
var seconds = 0;


//Allow to multiple popup
L.Map = L.Map.extend({
 	openPopup: function (popup, latlng, options) { 
        if (!(popup instanceof L.Popup)) {
        var content = popup;
        
        popup = new L.Popup(options).setContent(content);
        }
        
        if (latlng) {
        popup.setLatLng(latlng);
        }
        
        if (this.hasLayer(popup)) {
        return this;
        }
        
        //this.closePopup();
        this._popup = popup;
        return this.addLayer(popup);        
    }
});



//Adding icons for markers.
var boat_marker_icon_start = L.icon({
    iconUrl: 'images/boat_marker_start.png',

    iconSize:     [20, 20], // size of the icon
    //shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [12, 16], // point of the icon which will correspond to marker's location
    //shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -10] // point from which the popup should open relative to the iconAnchor
});

var boat_marker_icon_go = L.icon({
    iconUrl: 'images/boat_marker_go.png',

    iconSize:     [20, 20], // size of the icon
    //shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [12, 16], // point of the icon which will correspond to marker's location
    //shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -18] // point from which the popup should open relative to the iconAnchor
});

var station_marker_icon = L.icon({
    iconUrl: 'images/station_marker.png',

    iconSize:     [25, 25], // size of the icon
    //shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [12, 23], // point of the icon which will correspond to marker's location
    //shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -18] // point from which the popup should open relative to the iconAnchor
});


//Adding option for polygon.
var polylineOptions = {
               color: '#00b33c',
               weight: 5,
               opacity: 0.8
             };


//Setting up the main view of the map
var mymap = L.map('mapid').setView([14.5943653, 121.0228634], 12);


//Define the map dependent.
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
	maxZoom: 18,
	attribution: '<a style = "display:none" href="http://openstreetmap.org"></a> <a style = "display:none" href="http://creativecommons.org/licenses/by-sa/2.0/"></a><a style = "display:none" href="http://mapbox.com"></a>',
	id: 'mapbox.streets'
	}).addTo(mymap);

var station_1 = L.marker([14.6668, 120.94395],{icon: station_marker_icon}).addTo(mymap).bindPopup("<b class='station_style'>Station 1</b>").openPopup();


var xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");


//This function is use to update the current status of the map.
function current_location(){

	xmlhttp.onreadystatechange = function(){
		if(xmlhttp.readyState == 4 && xmlhttp.status == 200){
			var array = xmlhttp.responseText.toString().split("\n");
			var count = array.length;
			var cur_line = array[count-2].toString().split(" ");
			var prev_line = array[count-3].toString().split(" ");

			if(first_load != null){

				if(cur_line[2] != prev_line[2] || cur_line[3] != prev_line[3]){

					if(marker !== null){

						mymap.removeLayer(marker);

					}

					marker = L.marker([cur_line[2], cur_line[3]],{icon: boat_marker_icon_start}).addTo(mymap).bindPopup("<b class = 'pasig_popup'>Pasig Ferry</b>",{}).openPopup();

					passed_location(cur_line[2],cur_line[3]);
				}
			
			}else{
				first_load = "Not anymore";
				
				marker = L.marker([cur_line[2], cur_line[3]],{icon: boat_marker_icon_start}).addTo(mymap).bindPopup("<i class = 'pasig_popup'>Pasig Ferry</i>",{}).openPopup();

				passed_location(cur_line[2],cur_line[3]);
				
			}
			
			mymap.setView(new L.LatLng(cur_line[2], cur_line[3]));
			
			station_distance = distance(cur_line[2], cur_line[3], stations[0], stations[1], "K");
			
			time_duration = time(station_distance,min_speed);
			
			min_speed = speed(station_distance,time_duration);
			
			document.getElementById('distance').innerHTML = station_distance;
			
			document.getElementById('time').innerHTML = time_duration;
			
			document.getElementById('speed').innerHTML = min_speed;
			
		}	
	}

	xmlhttp.open("GET","output.txt", true);
	xmlhttp.send();
	
}


//Sub fuctions..
function passed_location(arrLat,arrLng){
	var passed_lat_lng = new L.LatLng(arrLat, arrLng);
	polylinePoints.push(passed_lat_lng);
	var polyline = new L.Polyline(polylinePoints, polylineOptions).addTo(mymap);
}


//Get Distance
function distance(lat1, lon1, lat2, lon2, unit) {
	var radlat1 = Math.PI * lat1/180
	var radlat2 = Math.PI * lat2/180
	var theta = lon1-lon2
	var radtheta = Math.PI * theta/180
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist)
	dist = dist * 180/Math.PI
	dist = dist * 60 * 1.1515
	if (unit=="K") { dist = dist * 1.609344 }
	if (unit=="N") { dist = dist * 0.8684 }
	return dist
}


//Get Time duration
function time(distance,speed){
	seconds = (distance * 1000) / (speed * 0.277777777777777777777777777777777777);
	hours = parseInt(seconds / 3600);
	seconds = seconds - (hours * 3600);
	minutes = parseInt(seconds / 60);
	seconds = parseInt(seconds - (minutes * 60));
	return hours + " : " + minutes + " : " + seconds;
}


function speed(distance,time){
	var temp = ((parseFloat(hours) * 3600) + (parseFloat(minutes) * 60) + parseFloat(seconds));
	return ((distance * 1000) / (temp * 0.277777777777777777777777777777777777));
}



function calculatespeed(form) {
//  get conversion factors from selected options
var i = form.distunits.selectedIndex;
var distunitsvalue = form.distunits.options[i].value; 
var j = form.speedunits.selectedIndex;
var speedunitsvalue = form.speedunits.options[j].value;
//  convert time to seconds
var temp = ((parseFloat(form.hourvalue.value) * 3600) + (parseFloat(form.minutevalue.value) * 60) + parseFloat(form.secondvalue.value));
//  calculate speed
form.speedvalue.value = ((form.distvalue.value * distunitsvalue)  / (temp * speedunitsvalue)); 
return true;
}


/*function calculatetime(form) {
//  get conversion factors from selected options
var i = form.distunits.selectedIndex;
var distunitsvalue = form.distunits.options[i].value; 
var j = form.speedunits.selectedIndex;
var speedunitsvalue = form.speedunits.options[j].value;
//  calculate time in seconds    
form.secondvalue.value = (form.distvalue.value * distunitsvalue) / (form.speedvalue.value * speedunitsvalue);
//  convert to hours, minutes, seconds    
form.hourvalue.value = parseInt(form.secondvalue.value / 3600);
form.secondvalue.value = form.secondvalue.value - (form.hourvalue.value * 3600);
form.minutevalue.value = parseInt(form.secondvalue.value / 60);
form.secondvalue.value = parseInt(form.secondvalue.value - (form.minutevalue.value * 60));
return true;
}*/


/*var popup = L.popup();

function onMapClick(e) {
	popup
		.setLatLng(e.latlng)
		.setContent("You clicked the map at " + e.latlng.toString())
		.openOn(mymap);
	}

//mymap.on('click', onMapClick); */
