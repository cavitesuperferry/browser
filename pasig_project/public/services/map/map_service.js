//Initialize map settings..

const DEFAULT_MAP_VIEW =  ["14.5550331","121.045429"];
const MIN_ZOOM = 12;
const MAX_ZOOM = 22;
const MIN_DISTANCE = 0.01; // 10 meters
const MIN_BOUNDS_TO_COUNT = 0.03 // 30 meters
const MIN_COUNT_STOP = 30// secs;

var temp_lat = 0;
var temp_lng = 0;
var temp_hour = 0;
var temp_min = 0;
var temp_sec = 0;
var temp_distance = 0;
var temp_speed = 0;
var temp_time = 0;
var hours = 0;
var minutes = 0;
var seconds = 0;
var isMoving = false;
var move_ctr = 0;

var station_list = [1,2,3,4,5,6,7,8,9,10,11];
var station_index = null;
var station_circle = [];
var station_marker = [];
var station_name = [];
var station_lat = [];
var station_lng = [];
var current_destination_data = [];
var next_destination_lat = [14.59523,14.5961,14.59606,14.59559,14.58243,14.58699,14.57424,14.56784,14.56849,14.55607,14.53554];
var next_destination_lng = [120.97512,120.97774,120.98163,121.01058,121.01132,121.01938,121.02608,121.03351,121.04785,121.07103,121.10209];
var current_destination = null;
var next_destination = null;
var end_destination = [1,11];
var from_left = [1,2,3,4,5,6,7,8,9,10,11];
var from_right = [1,2,3,4,5,6,7,8,9,10,11];

var mymap;
var myradar;
var polylinePoints = [];
var markerRadar = null;
var markerMap = null;
var first_load = null;


//Allow to multiple popup
L.Map = L.Map.extend({
 	openPopup: function (popup, latlng, options) { 
        if (!(popup instanceof L.Popup)) {
        var content = popup;
        
        popup = new L.Popup(options).setContent(content);
        }
        
        if (latlng) {
        popup.setLatLng(latlng);
        }
        
        if (this.hasLayer(popup)) {
        return this;
        }
        
        //this.closePopup();
        this._popup = popup;
        return this.addLayer(popup);        
    }
});

//Adding icons for markers.
var station_marker = L.icon({
    iconUrl: 'resources/img/station_marker.png',

    iconSize:     [25, 25], // size of the icon
    //shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [15, 25], // point of the icon which will correspond to marker's location
    //shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [2.5, -15] // point from which the popup should open relative to the iconAnchor
});

var marker_2 = L.icon({
    iconUrl: 'resources/img/radar_marker.ico',

    iconSize:     [35, 35], // size of the icon
    //shadowSize:   [50, 64], // size of the shadow
    iconAnchor:   [17, 32.5], // point of the icon which will correspond to marker's location
    //shadowAnchor: [4, 62],  // the same for the shadow
    popupAnchor:  [1, -10] // point from which the popup should open relative to the iconAnchor
});

//Adding option for polygon.
var polylineOptions = {
               color: '#00b33c',
               weight: 10,
               opacity: 0.9
             };


mymap = L.map('mapid').setView([DEFAULT_MAP_VIEW[0], DEFAULT_MAP_VIEW[1]], 13);
myradar = L.map('mapradar',{ zoomControl:false }).setView([DEFAULT_MAP_VIEW[0], DEFAULT_MAP_VIEW[1]], 16);

var xmlhttpRadar = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
var xmlhttpStation = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
var xmlhttpPoint = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
var xmlhttpRoute = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
var xmlhttpRoute_s_1_2 = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

//Define the map dependent.
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
	maxZoom: MAX_ZOOM,
	attribution: '<a style = "display:none" href="http://openstreetmap.org"></a> <a style = "display:none" href="http://creativecommons.org/licenses/by-sa/2.0/"></a><a style = "display:none" href="http://mapbox.com"></a>',
	id: 'mapbox.streets'
	}).addTo(mymap);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
	maxZoom: MAX_ZOOM,
	attribution: '<a style = "display:none" href="http://openstreetmap.org"></a> <a style = "display:none" href="http://creativecommons.org/licenses/by-sa/2.0/"></a><a style = "display:none" href="http://mapbox.com"></a>',
	id: 'mapbox.streets'
	}).addTo(myradar);
	
//Display point to point coordination with 30meters wide.


/*var c1 = L.circle([14.59569, 120.97717], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);

var c2 = L.circle([14.59636, 120.97839], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);

var c3 = L.circle([14.59689, 120.97971], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);

var c4 = L.circle([14.59654, 120.98105], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);

var c5 = L.circle([14.59475, 120.98281], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);


var c6 = L.circle([14.59313, 120.98389], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);

var c7 = L.circle([14.59133, 120.9894], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);

var c8 = L.circle([14.59115, 120.99215], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);


var c9 = L.circle([14.59203, 120.99408], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);

var c10 = L.circle([14.59406, 120.9956], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);

var c10 = L.circle([14.59517, 120.99636], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 30
}).addTo(mymap);

var c10 = L.circle([14.595453, 120.982172], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 0.1
}).addTo(mymap);

var c10 = L.circle([14.595376, 120.982221], {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5,
    radius: 0.1
}).addTo(mymap);

*/

	
var div = L.DomUtil.get('mapid');
if (!L.Browser.touch) {
    L.DomEvent.disableClickPropagation(div);
    L.DomEvent.on(div, 'mousewheel', L.DomEvent.stopPropagation);
} else {
    L.DomEvent.on(div, 'click', L.DomEvent.stopPropagation);
}


var popup = L.popup();

function onMapClick(e) {
	popup
		.setLatLng(e.latlng)
		.setContent(distance(temp_lat, temp_lng, e.latlng.lat, e.latlng.lng, "K") + "km<br/r>" + e.latlng.toString())
		.openOn(mymap);
	}
	
mymap.on('click', onMapClick);
