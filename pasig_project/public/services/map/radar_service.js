function current_location(){
	
	xmlhttpRadar.onreadystatechange = function(){
		if(xmlhttpRadar.readyState == 4 && xmlhttpRadar.status == 200){
			var array = xmlhttpRadar.responseText.toString().split("\n");
			var count = array.length;
			var cur_line = array[count-2].toString().split(" ");
			var prev_line = array[count-3].toString().split(" ");

			if(first_load != null){

				if(cur_line[2] != prev_line[2] || cur_line[3] != prev_line[3]){

					if(markerRadar !== null && markerMap !== null){

						myradar.removeLayer(markerRadar);
						mymap.removeLayer(markerMap);

					}

					markerRadar = L.marker([cur_line[2], cur_line[3]],{icon: marker_2}).addTo(myradar);
					
					markerMap = L.marker([cur_line[2], cur_line[3]],{icon: marker_2}).addTo(mymap);

					passed_location(cur_line[2],cur_line[3]);
					
					if(distance(temp_lat, temp_lng, cur_line[2], cur_line[3], "K") >= MIN_DISTANCE){
						
						$('#ds').text(current_destination_data[localStorage.getItem("station_index")]-temp_distance);
						$('#ns').text(current_destination_data[localStorage.getItem("station_index")]-temp_distance);
						$('#speed').text("Speed: " + speed(temp_distance,temp_hour,temp_min,temp_sec) + " Kmh");
						$('#time').text("Avg. Time: " + temp_hour + ":" + temp_min + ":" + temp_sec.toFixed(2));
						
						isMoving = true;
						$('#isMove').css({"background": "green"});
						$('#isMove').text("Status: Moving");
						
						CurGetHeading(cur_line[2],cur_line[3],prev_line[2],prev_line[3]);
						
						temp_lat = cur_line[2];
						temp_lng = cur_line[3];
						temp_hour = 0;
						temp_min = 0;
						temp_sec = 0;
						move_ctr = 0;
						
					}else{
						
						move_ctr++;
						
						
					}
					
				}
			
			}else{
				
				first_load = "Not anymore";
				
				temp_lat = cur_line[2];
				
				temp_lng = cur_line[3];
				
				markerRadar = L.marker([cur_line[2], cur_line[3]],{icon: marker_2}).addTo(myradar);
					
				markerMap = L.marker([cur_line[2], cur_line[3]],{icon: marker_2}).addTo(mymap);
				
				passed_location(cur_line[2],cur_line[3]);
				
			}
			
			if(move_ctr >= 120){
				$('#isMove').css({"background": "red"});
				$('#isMove').text("Status: Stopped");
				move_ctr = 0;
			}
			
			document.getElementById('lat').innerHTML = "Lat: " + cur_line[2];
			document.getElementById('lng').innerHTML = "Lng: " + cur_line[3];
			
			myradar.setView(new L.LatLng(cur_line[2], cur_line[3]));
			
		}	
	}

	xmlhttpRadar.open("GET","output.txt", true);
	xmlhttpRadar.send();
	
}

// Draw line base on passed area
function passed_location(arrLat,arrLng){
	var passed_lat_lng = new L.LatLng(arrLat, arrLng);
	polylinePoints.push(passed_lat_lng);
	var polyline = new L.Polyline(polylinePoints, polylineOptions).addTo(myradar);
}

// Get Distance 1
function distance(lat1, lon1, lat2, lon2, unit) {
	var radlat1 = Math.PI * lat1/180
	var radlat2 = Math.PI * lat2/180
	var theta = lon1-lon2
	var radtheta = Math.PI * theta/180
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist)
	dist = dist * 180/Math.PI
	dist = dist * 60 * 1.1515
	if (unit=="K") { dist = dist * 1.609344 }
	if (unit=="N") { dist = dist * 0.8684 }
	return temp_distance = dist.toFixed(2);
}

// Get Distance 2
function distance2(lat1, lon1, lat2, lon2, unit) {
	var radlat1 = Math.PI * lat1/180
	var radlat2 = Math.PI * lat2/180
	var theta = lon1-lon2
	var radtheta = Math.PI * theta/180
	var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
	dist = Math.acos(dist)
	dist = dist * 180/Math.PI
	dist = dist * 60 * 1.1515
	if (unit=="K") { dist = dist * 1.609344 }
	if (unit=="N") { dist = dist * 0.8684 }
	return temp_distance2 = dist.toFixed(2);
}

// Get Speed
function speed(distance,temp_hour,temp_min,temp_sec){
	var temp = ((parseFloat(temp_hour) * 3600) + (parseFloat(temp_min) * 60) + parseFloat(temp_sec));
	return temp_speed = ((distance * 1000) / (temp * 0.277777777777777777777777777777777777)).toFixed(2);
}


//Get Time duration
function time(distance,speed){
	seconds = (distance * 1000) / (speed * 0.277777777777777777777777777777777777);
	hours = parseInt(seconds / 3600);
	seconds = seconds - (hours * 3600);
	minutes = parseInt(seconds / 60);
	seconds = parseInt(seconds - (minutes * 60));
	return hours + " : " + minutes + " : " + seconds;
}

// Create Timer
function timer(){
	if(temp_sec < 59){
		temp_sec = temp_sec + 0.1;
	}else{
		temp_sec = 0;
		if(temp_min < 59){
			temp_min = temp_min + 1;
		}else{
			temp_sec = 0;
			temp_min = 0;
			if(temp_hour < 59){
				temp_hour = temp_hour + 1;
			}
		}
	}
	document.getElementById('timers').innerHTML = temp_hour+"h:"+temp_min+"m:"+temp_sec+"s";
}

