function getRoutesS1to2(){
	var line = {
	"type": "FeatureCollection",
	"features": [
		{
		"type": "Feature",
		"properties": {},
		"geometry": {
			"type": "LineString",
			"coordinates": [
				[14.59532, 120.97591],
				[14.59561, 120.97709],
			]
		}
		}
	]
	};

	var start = {
	"type": "Feature",
	"properties": {},
	"geometry": {
		"type": "Point",
		"coordinates": [14.5960082498,120.977769695]
	}
	};
	var stop = {
	"type": "Feature",
	"properties": {},
	"geometry": {
		"type": "Point",
		"coordinates": [14.5952161955,120.974946216]
	}
	};

	var sliced = turf.lineSlice(start, stop, line.features[0]);
	var length = turf.lineDistance(sliced, 'kilometers');
	
}
